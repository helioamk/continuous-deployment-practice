var express = require('express');

var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  var data  = "<h1>Hello webhook</h1>";
  res.send(data);
});

app.listen(3001, () => {
    console.log(`Example app listening at http://localhost:${3001}`)
})